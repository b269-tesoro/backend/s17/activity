// console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function getFullName(){
	let fullName = prompt("What is your name?");
	let age = prompt("How old are you?");
	let location = prompt("Where do you live?");
	console.log("Hello, "+ fullName);
	console.log("You are "+ age + " years old");
	console.log("You live in "+ location);


}
getFullName();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function printTopBands() {
	let topBand = ["The Beatles", "Metallica", "The Eagles", "L'arc~en~Ciel", "Eraserheads"]

	for (var i = 0; i < 5; i++) {
		console.log(i+1 +". " + topBand[i]);
;
	}
}
printTopBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function printTopMovies() {
	let topMovies = ["Your Name", "Knives Out", "Avengers:Endgame", "Spider-Man:No Way Home", "Soul"];
	let rottenTomatoRatings = ["98%","97%","94%","93%","95%"]

	for (var i = 0; i <5; i++) {
		console.log(i+1 +". " + topMovies[i]);
		console.log("Rotten Tomatoes Rating: " + rottenTomatoRatings[i]);
	}
}

printTopMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function /*printUsers*/(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);
